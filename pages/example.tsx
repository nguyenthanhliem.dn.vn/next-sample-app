import useTranslation from 'next-translate/useTranslation'

export default function ExamplePage() {
  const { t, lang } = useTranslation('common')
  const example = t('error-with-status', { statusCode: 42 })

  return <div>{example}</div> // <div>Using a variable 42</div>
}