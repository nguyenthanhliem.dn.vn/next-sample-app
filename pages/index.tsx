import Link from "next/link";
import useTranslation from 'next-translate/useTranslation'

export default function Homepage(props) {
  const { t, lang } = useTranslation('common');

  return (
  <>
    <main>
      <div>
        <button
          type="button"
          onClick={() => {console.log("xxxx")}}
        >
          {t("change-locale")}
        </button>
        <Link href="/ssr">
          <button type="button">{t("to-second-page")}</button>
        </Link>
      </div>
    </main>
  </>)
}