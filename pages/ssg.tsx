import { initialState } from "../store/reducers";
import { useSelector } from "react-redux";
import { RootState } from "../store/reducers";

export default function SSG() {
  const username = useSelector((state: RootState) => state.system.userName);
  return <div>hello {username}</div>;
}

// If you build and start the app, the date returned here will have the same
// value for all requests, as this method gets executed at build time.
export function getStaticProps() {
  // Note that in this case we're returning the state directly, without creating
  // the store first (like in /pages/ssr.js), this approach can be better and easier
  return {
    props: {
      initialReduxState: initialState,
    },
  };
}
