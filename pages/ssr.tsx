import { initializeStore } from "../store/";
import { sendMessage } from "../store/chat/actions";
import { useSelector } from "react-redux";
import { RootState } from "../store/reducers";
import useTranslation from 'next-translate/useTranslation'

export default function SSR() {
  const { t, lang } = useTranslation('common')
  const username = useSelector((state: RootState) => state.system.userName);
  return <p>{t("h1")} {username}</p>;
}

// The date returned here will be different for every request that hits the page,
// that is because the page becomes a serverless function instead of being statically
// exported when you use `getServerSideProps` or `getInitialProps`
export function getServerSideProps() {
  const reduxStore = initializeStore();
  const { dispatch } = reduxStore;

  dispatch(sendMessage({ user: "liem", message: "xxxxx", timestamp: 123321 }));

  return { props: { initialReduxState: reduxStore.getState() } };
}
