import { combineReducers } from "redux";
import {
  systemReducer,
  initialState as SystemInitialState,
} from "./system/reducers";
import { chatReducer, initialState as ChatInitialState } from "./chat/reducers";

export const rootReducer = combineReducers({
  system: systemReducer,
  chat: chatReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export const initialState: RootState = {
  system: SystemInitialState,
  chat: ChatInitialState,
};
