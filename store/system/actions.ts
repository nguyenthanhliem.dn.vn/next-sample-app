import { SystemState } from './types'
import { UPDATE_SESSION, SystemActionTypes } from './action-types'

export function updateSession(newSession: SystemState): SystemActionTypes {
  return {
    type: UPDATE_SESSION,
    payload: newSession
  }
}