import { SystemState } from "./types";
import { SystemActionTypes, UPDATE_SESSION } from "./action-types";

export const initialState: SystemState = {
  loggedIn: false,
  session: "",
  userName: "thanhliem",
};

export function systemReducer(
  state = initialState,
  action: SystemActionTypes
): SystemState {
  switch (action.type) {
    case UPDATE_SESSION: {
      return {
        ...state,
        ...action.payload,
      };
    }
    default:
      return state;
  }
}
